<article id="post-<?php the_ID(); ?>" <?php post_class('klas'); ?>>
  <header class="entry-header">
    <h1><?php esc_html_e( '404 - Page Not Found', 'wphierarchy' ); ?></h1>
  </header>
  <div class="entry-content">
    <p><?php esc_html_e( 'No content found', 'wphierarchy' ) ?></p>
    <p><?php echo get_search_form(); ?></p>
  </div>
</article>