<article id="post-<?php the_ID(); ?>" <?php post_class('post'); ?>>
  <header class="entry-header">
    <?php the_title('<h1>', '</h1>'); ?>
    <h3>From index.php</h3>
  </header>
  <div class="entry-content">
    <?php the_content(); ?>
  </div>
</article>