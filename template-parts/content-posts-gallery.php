<article id="post-<?php the_ID(); ?>" <?php post_class('klas'); ?>>
  <header class="entry-header">
   
    <!-- Post format -->
    <span class="dashicons dashicons-format-<?php echo get_post_format($post->ID); ?>"></span>
    <p><?php esc_html_e( 'Enjoy this gallery post', 'wphierarchy' ); ?></p>
    
    <?php the_title('<h2><a href="' . esc_url(get_permalink()) .'">', '</a></h2>'); ?>

  </header>
  <div class="entry-content">
    <?php the_content(); ?>
  </div>

  <?php if(comments_open()): ?>   
    <?php comments_template(); ?>
  <?php endif; ?>

</article>