    </div>
    <!-- $content -->
    <footer id="colophon" class="site-footer">
      <a href="<?php echo esc_url(__('https://wordpress.org/', 'wphierarchy')); ?>">
        <?php printf(esc_html__('Powered by %s', 'wphierarchy'), 'WordPress'); ?>
      </a>
    </footer>
  </div>
  <!-- #page -->

  <?php wp_footer(); ?>

</body>
</html>

85